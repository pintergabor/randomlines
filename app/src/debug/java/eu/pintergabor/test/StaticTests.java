package eu.pintergabor.test;

import java.util.Locale;
import java.util.Random;

import android.util.Log;

import androidx.annotation.*;

public class StaticTests {

    /**
     * Draw a line.
     *
     * @param x0 Start X.
     * @param y0 Start Y.
     * @param x1 End X.
     * @param y1 End Y.
     * @return a line in SVG.
     */
    @SuppressWarnings("unused")
    @NonNull
    private static String line(float x0, float y0, float x1, float y1) {
        return String.format(Locale.ROOT,
            "M%.1f,%.1fL%.1f,%.1f",
            x0, y0, x1, y1);
    }

    @SuppressWarnings("unused")
    private static Random rnd = new Random();

    @SuppressWarnings("unused")
    @NonNull
    private static String randomLine() {
        return String.format("<path android:pathData=\"%s\" android:strokeColor=\"#%03X\" android:strokeWidth=\"0.2\" />",
            line(108f * rnd.nextFloat(), 108f * rnd.nextFloat(), 108f * rnd.nextFloat(), 108f * rnd.nextFloat()),
            (int) (0xFFF * rnd.nextFloat()));
    }

    /**
     * Make a launcher icon.
     */
    @SuppressWarnings("unused")
    public static void makeLauncherIcon() {
        for (int i = 0; i < 100; i++) {
            Log.d("LAUNCHERICON", randomLine());
        }
    }

}
