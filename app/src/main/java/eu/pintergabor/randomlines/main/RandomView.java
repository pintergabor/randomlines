package eu.pintergabor.randomlines.main;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.*;

import eu.pintergabor.randomlines.com.MainHub;
import eu.pintergabor.randomlines.ui.MainActivity;
import eu.pintergabor.util.view.AdditionalViewUtils;

/**
 * Full screen view containing the drawing.
 */
public final class RandomView extends View {

    // region // Constructors

    /**
     * As required + local init.
     */
    public RandomView(Context context) {
        super(context);
        init(null);
    }

    /**
     * As required + local init.
     */
    public RandomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    /**
     * As required + local init.
     */
    public RandomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    /**
     * As required + local init.
     */
    @RequiresApi(21)
    @SuppressWarnings("unused")
    public RandomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    // endregion

    /**
     * Called at the end of every constructor to initialize custom fields.
     *
     * @param attrs Attribute set.
     */
    private void init(@Nullable @SuppressWarnings("unused") AttributeSet attrs) {
        MainHub mainHub = getMainHub();
        View view = this;
        mainHub.setRedrawRequestListener(new MainHub.RedrawRequestListener() {
            @Override
            public void redrawRequest() {
                view.postInvalidate();
            }
        });
    }

    /**
     * Get {@link MainHub}.
     *
     * @return The only {@link MainHub} instance from {@link MainActivity}.
     */
    private @NonNull MainHub getMainHub() {
        Activity activity = AdditionalViewUtils.getActivity(this);
        if (activity instanceof MainActivity) {
            return ((MainActivity) activity).getMainHub();
        }
        throw new ClassCastException("Impossible");
    }

    /**
     * Start/restart calculator and tell coordinator (and calculator)
     * to start producing new size bitmaps.
     *
     * @param w    New width.
     * @param h    New height.
     * @param oldw Not used.
     * @param oldh Not used.
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // Tell coordinator (and calculator) to start producing new size bitmaps
        MainHub mainHub = getMainHub();
        mainHub.viewWidth = w;
        mainHub.viewHeight = h;
    }

    /**
     * Draw the same or the next bitmap.
     *
     * @param canvas On this canvas.
     */
    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        // Get the bitmap
        MainHub mainHub = getMainHub();
        Coordinator coordinator = mainHub.getCoordinator();
        if (coordinator != null) {
            coordinator.getDisp();
            // Draw the bitmap
            if (coordinator.disp != null) {
                canvas.drawBitmap(coordinator.disp, 0, 0, null);
            }
        }
    }

}
