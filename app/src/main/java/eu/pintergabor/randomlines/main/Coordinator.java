package eu.pintergabor.randomlines.main;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Handler;
import android.view.View;

import androidx.annotation.*;

import eu.pintergabor.randomlines.com.MainHub;

/**
 * Coordinate UI thread and {@link Calculator} thread.
 * <p>
 * {@link Coordinator} calls {@link MainHub#redrawRequest()}
 * to ask the UI thread to redraw the image.
 * <p>
 * In response to this UI thread calls {@link #getDisp()} from onDraw
 * and takes the posted bitmap.
 */
public final class Coordinator {

    // region // Constructor and link to MainHub.

    /**
     * Link to {@link MainHub}.
     */
    @SuppressWarnings("unused")
    private final MainHub mainHub;

    public Coordinator(@NonNull MainHub mainHub) {
        this.mainHub = mainHub;
    }

    // endregion

    // region // The view that draws the bitmap

    /**
     * Get the width of the {@link View} that draws the {@link Bitmap}.
     *
     * @return Width.
     */
    @SuppressWarnings("unused")
    public synchronized int getWidth() {
        return mainHub.viewWidth;
    }

    /**
     * Get the height of the {@link View} that draws the {@link Bitmap}.
     *
     * @return Height.
     */
    @SuppressWarnings("unused")
    public synchronized int getHeight() {
        return mainHub.viewHeight;
    }

    // endregion

    // region // Timer

    private final Handler timer = new Handler();

    /**
     * true if {@link #timer} is running.
     */
    private boolean timerRunning = false;

    /**
     * Minimum screen refresh time.
     */
    private final int TIMEOUT = 15;

    /**
     * When {@link #TIMEOUT} elapsed.
     */
    private Runnable onFinish = new Runnable() {
        @Override
        public void run() {
            timeout();
        }
    };

    // endregion

    // region // The bitmaps.

    /**
     * The currently displayed bitmap.
     * <p>
     * Outside {@link Coordinator} only the UI thread is allowed to access it.
     */
    public Bitmap disp;

    /**
     * The posted new bitmap.
     */
    private Bitmap ready;

    /**
     * The currently calculated bitmap.
     * <p>
     * Outside {@link Coordinator} only the calculator thread is allowed to access it.
     */
    public Bitmap calc;

    /**
     * A bitmap no longer needed and can be recycled.
     */
    private Bitmap recycle;

    // endregion

    // region // Draw.

    /**
     * Get the posted bitmap.
     * <p>
     * Called from the UI thread.
     * <p>
     * After this call {@link #disp} will contain the latest bitmap,
     * and allows the calculator thread to post the next bitmap.
     */
    public synchronized void getDisp() {
        // If a new bitmap is available
        if (ready != null) {
            // Move pointers around
            recycle = disp;
            disp = ready;
            ready = null;
        }
        // Now disp contains the latest bitmap
    }

    /**
     * Ask the UI thread to redraw the image.
     */
    private synchronized void drawRequest() {
        mainHub.redrawRequest();
    }

    /**
     * If a new bitmap is ready by the end of the refresh interval,
     * ask the UI thread to take it over and draw it on screen.
     * <p>
     * If not, then wait for the calculator thread to post a new bitmap,
     * and that will ask the UI thread to take it over and draw it on screen.
     */
    private synchronized void timeout() {
        // If a new bitmap is available
        if (ready != null) {
            // Ask the UI thread to draw it
            drawRequest();
        }
        timerRunning = false;
    }

    /**
     * Post the newly calculated {@link #calc} bitmap to the UI thread.
     * <p>
     * Called from the calculator thread.
     * <p>
     * After this call {@link #calc} will be not null, and the same size
     * as the displaying view, but may be empty.
     * <p>
     * If the timer is not running (never started, or already finished),
     * then ask the UI thread to take it over and draw it on screen,
     * and restart the timer for the next update.
     */
    public synchronized void putCalc() {
        if (0 < getWidth() && 0 < getHeight()) {
            // If there is space for the newly calculated bitmap
            if (ready == null) {
                // If recycle is null, or the wrong size, create a new, empty one
                if (recycle == null ||
                    recycle.getWidth() != getWidth() ||
                    recycle.getHeight() != getHeight()) {
                    recycle = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
                }
                // Copy contents of calc into recycle, which will be the new calc
                final Canvas canvas = new Canvas(recycle);
                if (calc != null &&
                    calc.getWidth() == getWidth() &&
                    calc.getHeight() == getHeight()) {
                    canvas.drawBitmap(calc, 0, 0, null);
                }
                // Move pointers around
                ready = calc;
                calc = recycle;
                recycle = null;
            }
            // Ask for redraw after the minimum refresh interval
            if (!timerRunning) {
                drawRequest();
                // Prepare for the next update
                timer.postDelayed(onFinish, TIMEOUT);
                timerRunning = true;
            }
        }
    }

    // endregion

}
