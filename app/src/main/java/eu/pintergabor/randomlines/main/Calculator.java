package eu.pintergabor.randomlines.main;

import android.graphics.Canvas;

import androidx.annotation.*;

import eu.pintergabor.common.ControlledThread;
import eu.pintergabor.randomlines.com.MainHub;
import eu.pintergabor.randomlines.drawings.Circles;
import eu.pintergabor.randomlines.drawings.Drawing;
import eu.pintergabor.randomlines.drawings.Lines;
import eu.pintergabor.randomlines.drawings.Rectangles;
import eu.pintergabor.randomlines.drawings.Smileys;
import eu.pintergabor.randomlines.ui.MainActivity;

/**
 * Calculator thread.
 */
public final class Calculator extends ControlledThread {

    // region // Constructor and link to MainHub.

    /**
     * Link to {@link MainHub}.
     */
    @SuppressWarnings("unused")
    private final MainHub mainHub;

    public Calculator(@NonNull MainHub mainHub) {
        this.mainHub = mainHub;
    }

    // endregion

    // region // Create drawings.

    /**
     * Current drawing type.
     */
    private String currentDrawingType;

    /**
     * Current drawing.
     */
    private Drawing drawing;

    /**
     * Create or change {@link #drawing} and store its type in {@link #currentDrawingType}.
     */
    private void createDrawing() {
        try {
            String drawingType = mainHub.drawingType;
            if (!drawingType.equals(currentDrawingType)) {
                switch (drawingType) {
                    case "lines":
                        drawing = new Lines(mainHub);
                        break;
                    case "circles":
                        drawing = new Circles(mainHub, false);
                        break;
                    case "filled_circles":
                        drawing = new Circles(mainHub, true);
                        break;
                    case "rectangles":
                        drawing = new Rectangles(mainHub, false);
                        break;
                    case "filled_rectangles":
                        drawing = new Rectangles(mainHub, true);
                        break;
                    case "smileys":
                        drawing = new Smileys(mainHub);
                        break;
                }
                currentDrawingType = drawingType;
            }
        } catch (NullPointerException e) {
            drawing = null;
        }

    }

    @Override
    public void run() {
        Canvas canvas;
        final Coordinator coordinator = mainHub.getCoordinator();
        while (isRunning()) {
            mayPause();
            if (isRunning()) {
                if (coordinator.calc != null) {
                    canvas = new Canvas(coordinator.calc);
                    createDrawing();
                    if (drawing != null) {
                        drawing.draw(canvas);
                    }
                }
                coordinator.putCalc();
            }
        }
    }

    // endregion

}
