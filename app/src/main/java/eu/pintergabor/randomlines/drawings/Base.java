package eu.pintergabor.randomlines.drawings;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.SparseArray;

import androidx.annotation.*;

import java.util.Random;

import eu.pintergabor.randomlines.com.MainHub;
import eu.pintergabor.randomlines.ui.helper.ManageViews;
import eu.pintergabor.util.touch.TouchPoint;
import eu.pintergabor.util.touch.TouchRecorder;

import static android.graphics.Color.*;

/**
 * Base class for all drawings.
 */
class Base {

    // region // Constructor and link to MainHub.

    /**
     * Link to {@link MainHub}.
     */
    @SuppressWarnings("unused")
    private final MainHub mainHub;

    public Base(@NonNull MainHub mainHub) {
        this.mainHub = mainHub;
    }

    // endregion

    // region // Common fields.

    /**
     * A pen to draw with.
     */
    final Paint pen = new Paint();

    /**
     * A random number generator.
     */
    final Random rnd = new Random();

    /**
     * Coordinates of a rectangle.
     */
    float x0, y0, x1, y1;

    /**
     * Touch point.
     */
    float tpx, tpy;

    // endregion

    // region // Utilities

    /**
     * Clear screen, if requested.
     */
    void clearScreen(@NonNull Canvas canvas) {
        if (getClearScreen()) {
            final int width = canvas.getWidth();
            final int height = canvas.getHeight();
            pen.setColor(WHITE);
            pen.setStyle(Paint.Style.FILL);
            canvas.drawRect(0, 0, width, height, pen);
        }
    }

    /**
     * Create a random color.
     *
     * @return Random color.
     */
    int randomColor() {
        return 0xFF000000 | rnd.nextInt(0x1000000);
    }

    /**
     * Create a random rectangle ({@link #x0}, {@link #y0}) -- ({@link #x1}, {@link #y1})
     * <p>
     * Sets 0 <= x0 < width, 0 <= y0 < height, 0 <= x1 < width, 0 <= y1 < height
     *
     * @param width  Max X.
     * @param height Max Y.
     */
    void createRandomRect(int width, int height) {
        x0 = width * rnd.nextFloat();
        y0 = height * rnd.nextFloat();
        x1 = width * rnd.nextFloat();
        y1 = height * rnd.nextFloat();
    }

    /**
     * Translate size of rectangle ({@link #x0}, {@link #y0}) -- ({@link #x1}, {@link #y1}).
     */
    void xlatSize() {
        final float cx = (x0 + x1) / 2f;
        final float cy = (y0 + y1) / 2f;
        // Translate
        x0 = cx - (cx - x0) * getSize();
        x1 = cx - (cx - x1) * getSize();
        y0 = cy - (cy - y0) * getSize();
        y1 = cy - (cy - y1) * getSize();
    }

    /**
     * Get touch point and store it in ({@link #tpx}, {@link #tpy})
     *
     * @param i Use i%size if there are more than one.
     * @return True if there is at least one touch point.
     */
    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    boolean getTouchPoint(int i) {
        boolean touched;
        final TouchRecorder touchRecorder = TouchRecorder.getInstance();
        synchronized (touchRecorder) {
            final SparseArray<TouchPoint> touches = touchRecorder.touches;
            final int size = touches.size();
            touched = 0 < size;
            if (touched) {
                TouchPoint p = touches.valueAt(i % size);
                tpx = p.x - getLeftMargin();
                tpy = p.y;
            }
        }
        return touched;
    }

    /**
     * Common touch translation
     */
    void xlatTouch() {
        final float cx = (x0 + x1) / 2f;
        final float cy = (y0 + y1) / 2f;
        final float ta = Math.min(1f,
            getTouchStrength() + getTouchRandom() * rnd.nextFloat());
        // Offset calculation
        final float ocx = (tpx - cx) * ta;
        final float ocy = (tpy - cy) * ta;
        // Translate
        x0 = x0 + ocx;
        y0 = y0 + ocy;
        x1 = x1 + ocx;
        y1 = y1 + ocy;
    }

    /**
     * Do all common things.
     *
     * <ol>
     * <li>Create a random rectangle</li>
     * <li>Apply size translation</li>
     * <li>Apply touch translation</li>
     * </ol>
     *
     * @param width  Max X.
     * @param height Max Y.
     * @param i      Use i%size if there are more than one touch points.
     * @return True if there is at least one touch point.
     */
    boolean doAllCommon(int width, int height, int i) {
        // Random rectangle
        createRandomRect(width, height);
        // Size translation
        xlatSize();
        // Touch point
        final boolean touched = getTouchPoint(i);
        // Touch translation
        if (touched) {
            xlatTouch();
        }
        // Return
        return touched;
    }

    // endregion

    // region // Access properties stored in MainHub

    /**
     * Get line width.
     *
     * @return Width.
     */
    float getLineWidth() {
        return mainHub.lineWidth;
    }

    /**
     * Get size.
     *
     * @return Relative size [0..1].
     */
    float getSize() {
        return mainHub.size;
    }

    /**
     * Get touch strength.
     *
     * @return Relative touch strength [0..1].
     */
    float getTouchStrength() {
        return mainHub.touchStrength;
    }

    /**
     * Get touch randomness.
     *
     * @return Relative touch randomness [0..1].
     */
    float getTouchRandom() {
        return mainHub.touchRandom;
    }

    /**
     * Get free running.
     *
     * @return True if free running is enabled.
     */
    boolean getFreeRunning() {
        return mainHub.freeRunning;
    }

    /**
     * Get clear screen.
     *
     * @return True if clear screen is requested.
     */
    boolean getClearScreen() {
        boolean ret = mainHub.clearScreen;
        mainHub.clearScreen = false;
        return ret;
    }

    // endregion

    // region // Access UI environment properties stored in ManageViews

    /**
     * To help translating touch point coordinates to bitmap coordinates.
     *
     * @return Main view offset from the left side of the screen.
     */
    int getLeftMargin() {
        return mainHub.mainOffset;
    }

    // endregion

}
