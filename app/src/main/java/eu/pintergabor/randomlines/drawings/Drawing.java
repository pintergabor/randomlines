package eu.pintergabor.randomlines.drawings;

import android.graphics.Canvas;

import androidx.annotation.*;

/**
 * A generalized random drawing.
 */
public interface Drawing {

    /**
     * Draw something on {@link Canvas}
     *
     * @param canvas {@link Canvas} to draw on.
     */
    void draw(@NonNull Canvas canvas);

}
