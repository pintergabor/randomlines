package eu.pintergabor.randomlines.drawings;

import android.graphics.Canvas;
import android.graphics.Paint;

import androidx.annotation.*;

import eu.pintergabor.randomlines.com.MainHub;

/**
 * Random circles.
 */
public class Circles extends OrderedBase implements Drawing {

    private final boolean filled;

    /**
     * Draw circles.
     *
     * @param filled Filled or not.
     */
    public Circles(@NonNull MainHub mainHub, boolean filled) {
        super(mainHub);
        this.filled = filled;
    }

    /**
     * Draw circles on {@link Canvas}.
     */
    @Override
    public void draw(@NonNull Canvas canvas) {
        clearScreen(canvas);
        final int width = canvas.getWidth();
        final int height = canvas.getHeight();
        boolean touched;
        final float linewidth = getLineWidth();
        for (int i = 0; i < 100; i++) {
            // Do all common things
            touched = doAllCommon(width, height, i);
            // Draw
            if (touched || getFreeRunning()) {
                final float cx = (x0 + x1) / 2f;
                final float cy = (y0 + y1) / 2f;
                final float radius = Math.min(x1 - x0, y1 - y0) / 2f;
                // Inside
                if (filled) {
                    pen.setColor(randomColor());
                    pen.setStyle(Paint.Style.FILL);
                    canvas.drawCircle(cx, cy, radius, pen);
                }
                // Outline
                if (0 < linewidth) {
                    pen.setColor(randomColor());
                    pen.setStyle(Paint.Style.STROKE);
                    pen.setStrokeWidth(linewidth);
                    canvas.drawCircle(cx, cy, radius, pen);
                }
            }
        }
    }

}
