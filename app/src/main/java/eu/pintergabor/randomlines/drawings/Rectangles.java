package eu.pintergabor.randomlines.drawings;

import android.graphics.Canvas;
import android.graphics.Paint;

import androidx.annotation.*;

import eu.pintergabor.randomlines.com.MainHub;

/**
 * Random rectangles.
 */
public class Rectangles extends OrderedBase implements Drawing {

    private final boolean filled;

    /**
     * Draw rectangles.
     *
     * @param filled Filled or not.
     */
    public Rectangles(@NonNull MainHub mainHub, boolean filled) {
        super(mainHub);
        this.filled = filled;
    }

    /**
     * Draw rectangles on {@link Canvas}.
     */
    @Override
    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    public void draw(@NonNull Canvas canvas) {
        clearScreen(canvas);
        final int width = canvas.getWidth();
        final int height = canvas.getHeight();
        boolean touched;
        final float linewidth = getLineWidth();
        for (int i = 0; i < 100; i++) {
            // Do all common things
            touched = doAllCommon(width, height, i);
            // Draw
            if (touched || getFreeRunning()) {
                // Inside
                if (filled) {
                    pen.setColor(randomColor());
                    pen.setStyle(Paint.Style.FILL);
                    canvas.drawRect(x0, y0, x1, y1, pen);
                }
                // Outline
                if (0 < linewidth) {
                    pen.setColor(randomColor());
                    pen.setStyle(Paint.Style.STROKE);
                    pen.setStrokeWidth(linewidth);
                    canvas.drawRect(x0, y0, x1, y1, pen);
                }
            }
        }
    }

}
