package eu.pintergabor.randomlines.drawings;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

import androidx.annotation.*;
import androidx.core.content.ContextCompat;

import eu.pintergabor.randomlines.R;
import eu.pintergabor.randomlines.com.MainHub;
import eu.pintergabor.randomlines.ui.MainActivity;

import static android.graphics.Color.*;

/**
 * Random smileys.
 */
public class Smileys extends OrderedBase implements Drawing {

    private final Drawable smileyHappy;
    private final Drawable smileySad;

    /**
     * Create smileys.
     */
    public Smileys(@NonNull MainHub mainHub) {
        super(mainHub);
        final MainActivity mainActivity = mainHub.getMainActivity();
        smileyHappy = ContextCompat.getDrawable(mainActivity, R.drawable.smiley_happy);
        smileySad = ContextCompat.getDrawable(mainActivity, R.drawable.smiley_sad);
    }

    /**
     * Draw circles on {@link Canvas}.
     */
    @Override
    public void draw(@NonNull Canvas canvas) {
        clearScreen(canvas);
        final int width = canvas.getWidth();
        final int height = canvas.getHeight();
        boolean touched;
        final float linewidth = getLineWidth();
        for (int i = 0; i < 10; i++) {
            // Do all common things
            touched = doAllCommon(width, height, i);
            // Sad or happy
            Drawable smiley = 0.3f < rnd.nextFloat() ? smileyHappy : smileySad;
            // Draw
            if (touched || getFreeRunning()) {
                final float cx = (x0 + x1) / 2f;
                final float cy = (y0 + y1) / 2f;
                final float radius = Math.min(x1 - x0, y1 - y0) / 2f;
                // Face inside
                pen.setColor(randomColor());
                pen.setStyle(Paint.Style.FILL);
                canvas.drawCircle(cx, cy, radius, pen);
                // Face outline
                if (0 < linewidth) {
                    pen.setColor(BLACK);
                    pen.setStyle(Paint.Style.STROKE);
                    pen.setStrokeWidth(linewidth);
                    canvas.drawCircle(cx, cy, radius, pen);
                }
                // Eyes and mouth
                smiley.setBounds(
                    (int) (cx - radius), (int) (cy - radius),
                    (int) (cx + radius), (int) (cy + radius));
                smiley.draw(canvas);
            }
        }
    }

}
