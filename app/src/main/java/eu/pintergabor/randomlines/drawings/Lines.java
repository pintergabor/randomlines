package eu.pintergabor.randomlines.drawings;

import android.graphics.Canvas;
import android.graphics.Paint;

import androidx.annotation.*;

import eu.pintergabor.randomlines.com.MainHub;

/**
 * Random lines.
 */
public class Lines extends Base implements Drawing {

    public Lines(@NonNull MainHub mainHub) {
        super(mainHub);
        pen.setStyle(Paint.Style.STROKE);
    }

    /**
     * Draw lines on {@link Canvas}.
     */
    @Override
    public void draw(@NonNull Canvas canvas) {
        clearScreen(canvas);
        final int width = canvas.getWidth();
        final int height = canvas.getHeight();
        boolean touched;
        pen.setStrokeWidth(getLineWidth());
        pen.setColor(randomColor());
        for (int i = 0; i < 100; i++) {
            // Do all common things
            touched = doAllCommon(width, height, i);
            // Draw
            if (touched || getFreeRunning()) {
                canvas.drawLine(x0, y0, x1, y1, pen);
            }
        }
    }

}
