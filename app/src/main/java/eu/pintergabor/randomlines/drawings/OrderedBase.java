package eu.pintergabor.randomlines.drawings;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.SparseArray;

import androidx.annotation.NonNull;

import java.util.Random;

import eu.pintergabor.randomlines.com.MainHub;
import eu.pintergabor.randomlines.ui.helper.ManageViews;
import eu.pintergabor.util.touch.TouchPoint;
import eu.pintergabor.util.touch.TouchRecorder;

import static android.graphics.Color.WHITE;

/**
 * Same as base class, but ensures that x0 <= x1 and y0 <= y1.
 */
class OrderedBase extends Base {

    public OrderedBase(@NonNull MainHub mainHub) {
        super(mainHub);
    }

    /**
     * Create a random rectangle ({@link #x0}, {@link #y0}) -- ({@link #x1}, {@link #y1})
     *
     * Sets 0 <= x0 <= x1 < width, 0 <= y0 <= y1 < height
     *
     * @param width  Max X.
     * @param height Max Y.
     */
    @Override
    void createRandomRect(int width, int height) {
        super.createRandomRect(width, height);
        // Ensure x0 <= x1
        if (x1 < x0) {
            final float x = x1;
            x1 = x0;
            x0 = x;
        }
        // Ensure y0 <= y1
        if (y1 < y0) {
            final float y = y1;
            y1 = y0;
            y0 = y;
        }
    }

}
