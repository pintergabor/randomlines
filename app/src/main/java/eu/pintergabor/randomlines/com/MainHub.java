package eu.pintergabor.randomlines.com;

import androidx.annotation.*;

import eu.pintergabor.randomlines.main.Calculator;
import eu.pintergabor.randomlines.main.Coordinator;
import eu.pintergabor.randomlines.main.RandomView;
import eu.pintergabor.randomlines.ui.MainActivity;
import eu.pintergabor.randomlines.ui.PrefsFragment;
import eu.pintergabor.randomlines.ui.helper.ManageViews;

/**
 * Communication interface between {@link PrefsFragment}, {@link RandomView},
 * {@link Coordinator} and {@link Calculator}.
 */
public final class MainHub {

    // region // Constructor and link to MainActivity.

    /**
     * Link to {@link MainActivity}.
     */
    @SuppressWarnings("unused")
    private final MainActivity mainActivity;

    public MainHub(@NonNull MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    /**
     * Get link to {@link MainActivity}.
     *
     * @return Link to {@link MainActivity}.
     */
    public MainActivity getMainActivity() {
        return mainActivity;
    }

    // endregion

    // region // Coordinator and Calculator.

    private Coordinator coordinator;

    /**
     * Coordinate UI and {@link Calculator} thread.
     */
    @SuppressWarnings("unused")
    public Coordinator getCoordinator() {
        return coordinator;
    }

    private Calculator calculator;

    /**
     * Calculate and post new bitmaps.
     */
    @SuppressWarnings("unused")
    public Calculator getCalculator() {
        return calculator;
    }

    /**
     * Create {@link Coordinator} and {@link Calculator} when the main application starts.
     */
    public void onResume() {
        coordinator = new Coordinator(this);
        calculator = new Calculator(this);
        // Start paused
        calculator.pauseRun();
        calculator.start();
    }

    /**
     * Destroy {@link Coordinator} and {@link Calculator} when the main application stops.
     */
    public void onPause() {
        if (calculator != null) {
            calculator.terminateRun();
            calculator = null;
        }
        if (coordinator != null) {
            coordinator = null;
        }
    }

    /**
     * Pause {@link Calculator} when the drawing view is not visible.
     */
    public void pauseCalculator() {
        if (calculator != null) {
            calculator.pauseRun();
        }
    }

    /**
     * Resume {@link Calculator} when the drawing view is visible.
     */
    public void resumeCalculator() {
        if (calculator != null) {
            calculator.resumeRun();
        }
    }

    // endregion

    // region // Redraw requests.

    private RedrawRequestListener redrawRequestListener;

    /**
     * Set callback on redraw requests.
     *
     * @param listener Callback.
     */
    public void setRedrawRequestListener(RedrawRequestListener listener) {
        redrawRequestListener = listener;
    }

    /**
     * Callback on redraw request.
     */
    public interface RedrawRequestListener {
        /**
         * Called on request redraw.
         */
        void redrawRequest();
    }

    /**
     * Request redraw.
     * <p>
     * {@link Coordinator} will call it when it has a new bitmap to display.
     */
    public void redrawRequest() {
        if (redrawRequestListener != null) {
            redrawRequestListener.redrawRequest();
        }
    }

    // endregion

    // region // Parameters that affect the drawing.

    /**
     * Main bitmap display offset from left side.
     * <p>
     * Set by {@link ManageViews}, but kept here to help translating
     * touch points to bitmap relative points.
     */
    public int mainOffset;

    /**
     * {@link RandomView} width.
     */
    public int viewWidth;

    /**
     * {@link RandomView} height.
     */
    public int viewHeight;

    /**
     * Drawing type.
     */
    public String drawingType;

    /**
     * Line width.
     */
    public float lineWidth;

    /**
     * Size.
     * <p>
     * [0..1]
     */
    public float size;

    /**
     * Touch strength.
     * <p>
     * [0..1]
     */
    public float touchStrength;

    /**
     * Touch randomness.
     * <p>
     * [0..1]
     */
    public float touchRandom;

    /**
     * Free running.
     */
    public boolean freeRunning;

    /**
     * Clear screen.
     */
    public boolean clearScreen;

    // endregion

}
