package eu.pintergabor.randomlines.ui.helper;

import android.content.SharedPreferences;
import android.view.KeyEvent;
import android.view.MenuItem;

import androidx.annotation.*;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.preference.ListPreference;
import androidx.preference.PreferenceManager;

import org.jetbrains.annotations.*;

import eu.pintergabor.randomlines.R;
import eu.pintergabor.randomlines.com.MainHub;
import eu.pintergabor.randomlines.ui.MainActivity;
import eu.pintergabor.randomlines.ui.PrefsFragment;
import eu.pintergabor.randomlines.ui.SettingsFragment;
import eu.pintergabor.util.preferences.ValueToSummary;

/**
 * Help {@link MainActivity} with handling buttons.
 */
public final class Buttons extends AutoHideToolbar implements Toolbar.OnMenuItemClickListener {

    // region // Constructor and link to MainActivity.

    /**
     * Configure buttons.
     */
    public Buttons(MainActivity mainActivity) {
        super(mainActivity);
        // Set up main toolbar
        inflateMenu(R.id.toolbar, R.menu.toolbar);
        // Set up small toolbars in Settings and About
        inflateMenu(R.id.settings_toolbar, R.menu.toolbar_back);
        inflateMenu(R.id.about_toolbar, R.menu.toolbar_back);
        // Show buttons in the beginning
        delayHide();
    }

    // endregion

    // region // Utils.

    /**
     * Inflate menu, set onClickListener, and enable icons in overflow menu.
     *
     * @param toolbarid Toolbar id.
     * @param menures   Menu id of the toolbar.
     */
    private void inflateMenu(@IdRes int toolbarid, @MenuRes int menures) {
        Toolbar toolbar = mainActivity.findViewById(toolbarid);
        MenuBuilder menu = (MenuBuilder) toolbar.getMenu();
        // Inflate toolbar menu.
        menu.clear();
        toolbar.inflateMenu(menures);
        // Set onClickListener.
        toolbar.setOnMenuItemClickListener(this);
        // Show icons in overflow menu.
        menu = (MenuBuilder) toolbar.getMenu();
        menu.setOptionalIconsVisible(true);
    }

    // endregion


    // region // View state change button and keyboard key actions

    /**
     * Change view state to SETTINGS or SETTINGS_MAIN.
     * <p>
     * Used to enter SETTINGS.
     */
    public void toSettings() {
        final ManageViews manageViews = mainActivity.getManageViews();
        final ManageViews.VIEWSTATE cvs = manageViews.getCurrentViewstate();
        final boolean portrait = manageViews.portrait();
        ManageViews.VIEWSTATE vs = ManageViews.VIEWSTATE.SETTINGS;
        switch (cvs) {
            case SETTINGS_MAIN:
            case ABOUT_MAIN:
            case MAIN:
                if (!portrait) {
                    vs = ManageViews.VIEWSTATE.SETTINGS_MAIN;
                }
                break;
        }
        manageViews.setCurrentViewstate(vs);
    }

    /**
     * Change view state to SETTINGS or SETTINGS_MAIN.
     * <p>
     * Used to enter ABOUT.
     */
    public void toAbout() {
        final ManageViews manageViews = mainActivity.getManageViews();
        final ManageViews.VIEWSTATE cvs = manageViews.getCurrentViewstate();
        final boolean portrait = manageViews.portrait();
        ManageViews.VIEWSTATE vs = ManageViews.VIEWSTATE.ABOUT;
        switch (cvs) {
            case SETTINGS_MAIN:
            case ABOUT_MAIN:
            case MAIN:
                if (!portrait) {
                    vs = ManageViews.VIEWSTATE.ABOUT_MAIN;
                }
                break;
        }
        manageViews.setCurrentViewstate(vs);
    }

    /**
     * Change view state to MAIN or to SETTINGS_MAIN or to ABOUT_MAIN.
     * <p>
     * Used to exit SETTINGS and ABOUT.
     */
    public void toMain() {
        final ManageViews manageViews = mainActivity.getManageViews();
        final ManageViews.VIEWSTATE cvs = manageViews.getCurrentViewstate();
        final boolean portrait = manageViews.portrait();
        ManageViews.VIEWSTATE vs = ManageViews.VIEWSTATE.MAIN;
        switch (cvs) {
            case SETTINGS:
            case SETTINGS_MAIN:
                if (!portrait) {
                    manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.SETTINGS_MAIN);
                }
                break;
            case ABOUT:
            case ABOUT_MAIN:
                if (!portrait) {
                    manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.ABOUT_MAIN);
                }
                break;
        }
        manageViews.setCurrentViewstate(vs);
    }

    /**
     * Call from {@link MainActivity} when the back button is pressed.
     */
    public boolean onBackPressed() {
        final ManageViews manageViews = mainActivity.getManageViews();
        final ManageViews.VIEWSTATE cvs = manageViews.getCurrentViewstate();
        final boolean portrait = manageViews.portrait();
        switch (cvs) {
            case ABOUT:
                if (portrait) {
                    manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.MAIN);
                } else {
                    manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.ABOUT_MAIN);
                }
                return true;
            case SETTINGS:
                if (portrait) {
                    manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.MAIN);
                } else {
                    manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.SETTINGS_MAIN);
                }
                return true;
            case ABOUT_MAIN:
            case SETTINGS_MAIN:
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.MAIN);
                return true;
        }
        return false;
    }

    /**
     * Call from {@link MainActivity} when a key is is pressed.
     */
    public boolean onKeyDown(int keyCode, @SuppressWarnings("unused") KeyEvent event) {
        final ManageViews manageViews = mainActivity.getManageViews();
        switch (keyCode) {
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_F1:
            case KeyEvent.KEYCODE_A:
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.ABOUT);
                return true;
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_F2:
            case KeyEvent.KEYCODE_S:
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.SETTINGS);
                return true;
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_F3:
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.ABOUT_MAIN);
                return true;
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_F4:
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.SETTINGS_MAIN);
                return true;
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_F5:
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_NUMPAD_ENTER:
            case KeyEvent.KEYCODE_W:
            case KeyEvent.KEYCODE_M:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                manageViews.setCurrentViewstate(ManageViews.VIEWSTATE.MAIN);
                return true;
        }
        return false;
    }

    @Override
    public boolean onMenuItemClick(@NonNull MenuItem item) {
        delayHide();
        switch (item.getItemId()) {
            case R.id.action_settings:
                toSettings();
                break;
            case R.id.action_clear:
                final MainHub mainHub = mainActivity.getMainHub();
                mainHub.clearScreen = true;
                break;
            case R.id.action_lines:
                selectDrawing("lines");
                break;
            case R.id.action_circles:
                selectDrawing("circles");
                break;
            case R.id.action_filled_circles:
                selectDrawing("filled_circles");
                break;
            case R.id.action_rectangles:
                selectDrawing("rectangles");
                break;
            case R.id.action_filled_rectangles:
                selectDrawing("filled_rectangles");
                break;
            case R.id.action_smileys:
                selectDrawing("smileys");
                break;
            case R.id.action_about:
                toAbout();
                break;
            case R.id.action_back:
                toMain();
                break;
            case R.id.action_close:
                mainActivity.finish();
                break;
        }
        return true;
    }

    // endregion

    // region // Select a drawing.

    /**
     * Select a drawing.
     *
     * @param drawingType Name of the drawing.
     */
    @SuppressWarnings("ConstantConditions")
    private void selectDrawing(String drawingType) {
        final MainHub mainHub = mainActivity.getMainHub();
        // Change the value in shared preferences
        SharedPreferences sp = PreferenceManager.
            getDefaultSharedPreferences(mainActivity);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("drawing_type", drawingType);
        editor.apply();
        // Change the value in preference fragment, if that is active.
        try {
            // Get SettingsFragment
            FragmentManager fragmentManager = mainActivity.getSupportFragmentManager();
            SettingsFragment settingsFragment = (SettingsFragment) fragmentManager.
                findFragmentById(R.id.fragment_settings);
            // Get PrefsFragment, which is inside SettingsFragment
            fragmentManager = settingsFragment.getChildFragmentManager();
            PrefsFragment prefsFragment = (PrefsFragment) fragmentManager.
                findFragmentById(R.id.fragment_prefs);
            // Find the Preference
            ListPreference drawingPreference = prefsFragment.
                findPreference("drawing_type");
            // Set it
            drawingPreference.setValue(drawingType);
            ValueToSummary.updateSummary(drawingPreference, drawingType);
        } catch (NullPointerException e) {
            // OK, it is probably inactive.
        }
        // Apply it to the drawing
        mainHub.drawingType = drawingType;
    }

    // endregion

}
