package eu.pintergabor.randomlines.ui;

import android.os.Bundle;

import androidx.annotation.*;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import eu.pintergabor.randomlines.BuildConfig;
import eu.pintergabor.randomlines.R;

/**
 * About.
 */
public class AboutFragment extends Fragment {

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public View onCreateView(
        @NonNull LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_about, container, false);
        // Set version number
        final TextView version = v.findViewById(R.id.about_version);
        version.setText(BuildConfig.VERSION_NAME);
        // Done
        return v;
    }

}
