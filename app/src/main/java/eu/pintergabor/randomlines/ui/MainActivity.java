package eu.pintergabor.randomlines.ui;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;

import androidx.annotation.*;
import androidx.appcompat.app.AppCompatActivity;

import org.jetbrains.annotations.*;

import eu.pintergabor.randomlines.com.MainHub;
import eu.pintergabor.randomlines.R;
import eu.pintergabor.randomlines.ui.helper.Buttons;
import eu.pintergabor.randomlines.ui.helper.ManageViews;
import eu.pintergabor.util.runcount.RunCount;
import eu.pintergabor.util.touch.TouchRecorder;

/**
 * Main program
 */
public class MainActivity extends AppCompatActivity {

    // region // "Globals".

    private MainHub mainHub;

    /**
     * Communication interface.
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    @NonNull
    public MainHub getMainHub() {
        return mainHub;
    }

    private ManageViews manageViews;

    /**
     * Manage the visibility and size of views.
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    @NonNull
    public ManageViews getManageViews() {
        return manageViews;
    }

    /**
     * Handling buttons and keys.
     */
    private Buttons buttons;


    // endregion

    // region // Lifecycle events.

    /**
     * Start of the program.
     *
     * @param savedInstanceState Previous state.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Restore state
        super.onCreate(savedInstanceState);
        // Load, and increment, run counter
        RunCount.getInstance().load(this).increment();
        // Create communication interface
        mainHub = new MainHub(this);
        // Main layout
        setContentView(R.layout.activity_main);
        // Manage visibility of UI elements
        manageViews = new ManageViews(this);
        // Buttons visibility and control handler
        buttons = new Buttons(this);
        // Create launcher icon (empty in "release" build)
        // StaticTests.makeLauncherIcon();
    }

    /**
     * Prepare to show main view.
     */
    @Override
    protected void onResume() {
        super.onResume();
        // Start updating UI
        mainHub.onResume();
        // Restore last view state
        manageViews.refreshCurrentViewstate();
    }

    /**
     * Save state.
     */
    @Override
    protected void onPause() {
        super.onPause();
        // No need to update UI
        mainHub.onPause();
        // Save the already incremented run counter
        RunCount.getInstance().save(this);
    }

    // endregion

    // region // Touch, key and permission hooks.

    /**
     * Hook {@link TouchRecorder} into touch events.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Keep track of touch pointers
        TouchRecorder.getInstance().onTouchEvent(event);
        // Show and delay hide buttons
            buttons.delayHide();
        return true;
    }

    /**
     * Hook {@link Buttons#onBackPressed()} into back button processing.
     */
    @Override
    public void onBackPressed() {
        if (!buttons.onBackPressed()) {
            super.onBackPressed();
        }
    }

    /**
     * Hook {@link Buttons#onKeyDown(int, KeyEvent)} into keypress processing.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (buttons.onKeyDown(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    // endregion

}
