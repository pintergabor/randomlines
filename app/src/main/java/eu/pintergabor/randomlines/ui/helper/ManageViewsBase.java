package eu.pintergabor.randomlines.ui.helper;

import android.view.View;

import org.jetbrains.annotations.*;

import eu.pintergabor.randomlines.R;
import eu.pintergabor.randomlines.ui.MainActivity;


/**
 * Keep track of full screen (full frame) properties.
 */
public abstract class ManageViewsBase {

    // region // Keep track of full screen (full frame) properties.

    /**
     * Link to {@link MainActivity}.
     */
    protected final MainActivity mainActivity;

    /**
     * Register layout change listener to keep track of full screen (full frame) size changes.
     */
    public ManageViewsBase(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        frameDensity = mainActivity.getResources().getDisplayMetrics().density;
        View mainFrame = mainActivity.findViewById(R.id.frame_main);
        mainFrame.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            /**
             * Get display size and update
             * {@link ManageViewsBase#getFrameWidth()},
             * {@link ManageViewsBase#getFrameHeight()} and
             * {@link ManageViewsBase#getFrameDensity()}.
             */
            @Override
            public void onLayoutChange(View view,
                                       int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                final int w = right - left;
                final int h = bottom - top;
                if (frameWidth != w || frameHeight != h) {
                    frameWidth = w;
                    frameHeight = h;
                    refreshCurrentViewstate();
                }
            }
        });
    }

    /**
     * Refresh current view state, if frame size or orientation changes.
     */
    public abstract void refreshCurrentViewstate();

    // endregion

    // region // Full screen (full frame) properties.

    private int frameWidth;

    /**
     * Frame width.
     * (= Display width in full screen mode.)
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    public int getFrameWidth() {
        return frameWidth;
    }

    private int frameHeight;

    /**
     * Frame height.
     * (= Display height in full screen mode.)
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    public int getFrameHeight() {
        return frameHeight;
    }

    private float frameDensity;

    /**
     * Frame resolution.
     */
    @SuppressWarnings("unused")
    @Contract(pure = true)
    public float getFrameDensity() {
        return frameDensity;
    }

    /**
     * @return true if the application frame is portrait, or near portrait.
     */
    @Contract(pure = true)
    public boolean portrait() {
        return !((frameWidth - frameHeight) / frameDensity > 200);
    }

    // endregion

}
