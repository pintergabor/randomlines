package eu.pintergabor.randomlines.ui.helper;

import android.os.Build;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.*;

import org.jetbrains.annotations.*;

import eu.pintergabor.randomlines.R;
import eu.pintergabor.randomlines.com.MainHub;
import eu.pintergabor.randomlines.ui.MainActivity;

/**
 * Manage views displayed in the main application frame.
 */
public final class ManageViews extends ManageViewsBase {

    // region // Constructor.

    public ManageViews(MainActivity mainActivity) {
        super(mainActivity);
    }

    // endregion

    // region // View states.

    /**
     * All possible view states.
     *
     * <table>
     * <tr><td>MAIN</td><td> - </td><td>Main only</td></tr>
     * <tr><td>SETTINGS</td><td> - </td><td>Settings only</td></tr>
     * <tr><td>ABOUT</td><td> - </td><td>About only</td></tr>
     * <tr><td>SETTINGS_MAIN</td><td> - </td><td>Settings and main</td></tr>
     * <tr><td>ABOUT_MAIN</td><td> - </td><td>About and main</td></tr>
     * </table>
     * <p>
     * In portrait, or near portrait mode only SETTINGS, MAIN and ABOUT are used.
     */
    public enum VIEWSTATE {
        MAIN,
        SETTINGS,
        ABOUT,
        SETTINGS_MAIN,
        ABOUT_MAIN,
    }

    /**
     * Current view state.
     */
    private VIEWSTATE currentViewstate = VIEWSTATE.MAIN;

    /**
     * Set size of a {@link View}.
     *
     * @param view  The {@link View}.
     * @param width Desired width in pixels.
     * @param height Desired height in pixels.
     */
    private void setSize(@NonNull View view, int width, int height) {
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = width;
        params.height = height;
        view.setLayoutParams(params);
    }

    /**
     * Set width of a {@link View}.
     *
     * @param view   The {@link View}.
     * @param offset Desired offset in pixels.
     */
    private void setOffset(@NonNull View view, int offset) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        params.leftMargin = offset;
        params.rightMargin = -offset;
        view.setLayoutParams(params);
    }

    /**
     * Get current view state.
     *
     * @return Current {@link VIEWSTATE}.
     */
    @SuppressWarnings("unused")
    public VIEWSTATE getCurrentViewstate() {
        return currentViewstate;
    }

    /**
     * Change view state.
     *
     * @param v New {@link VIEWSTATE}.
     */
    @SuppressWarnings("unused")
    public void setCurrentViewstate(VIEWSTATE v) {
        // Get references to the embedded views
        final View container = mainActivity.findViewById(R.id.container_main);
        final View settings = mainActivity.findViewById(R.id.fragment_settings);
        final View main = mainActivity.findViewById(R.id.fragment_main);
        final View about = mainActivity.findViewById(R.id.fragment_about);
        final MainHub mainHub = mainActivity.getMainHub();
        // SETTINGS_MAIN and ABOUT_MAIN states are invalid in portrait mode.
        if (portrait() &&
            (v == VIEWSTATE.SETTINGS_MAIN || v == VIEWSTATE.ABOUT_MAIN)) {
            v = VIEWSTATE.MAIN;
        }
        // Request animation
        if (21 <= Build.VERSION.SDK_INT) {
            ViewGroup fullscreen = mainActivity.findViewById(R.id.frame_main);
            TransitionManager.beginDelayedTransition(fullscreen);
        }
        // Change the size of the embedded views.
        final int w = getFrameWidth();
        final int h = getFrameHeight();
        final int r = w - h;
        switch (v) {
            case MAIN:
                setSize(container, 2 * w, h);
                setSize(settings, w, h);
                setSize(about, w, 0);
                setSize(main, w, h);
                setOffset(container, -w);
                main.requestFocus();
                mainHub.mainOffset = 0;
                mainHub.resumeCalculator();
                break;
            case SETTINGS:
                setSize(container, 2 * w, h);
                setSize(settings, w, h);
                setSize(about, w, 0);
                setSize(main, w, h);
                setOffset(container, 0);
                mainHub.pauseCalculator();
                break;
            case ABOUT:
                setSize(container, 2 * w, h);
                setSize(settings, w, 0);
                setSize(about, w, h);
                setSize(main, w, h);
                setOffset(container, 0);
                mainHub.pauseCalculator();
                break;
            case SETTINGS_MAIN:
                setSize(container, w, h);
                setSize(settings, r, h);
                setSize(about, r, 0);
                setSize(main, h, h);
                setOffset(container, 0);
                mainHub.mainOffset = r;
                mainHub.resumeCalculator();
                break;
            case ABOUT_MAIN:
                setSize(container, w, h);
                setSize(settings, r, 0);
                setSize(about, r, h);
                setSize(main, h, h);
                setOffset(container, 0);
                mainHub.mainOffset = r;
                mainHub.resumeCalculator();
                break;
        }
        // Remember the view state.
        currentViewstate = v;
    }

    /**
     * Refresh current view state if frame size or orientation changes.
     */
    @SuppressWarnings("unused")
    public void refreshCurrentViewstate() {
        setCurrentViewstate(currentViewstate);
    }

    // endregion

}
