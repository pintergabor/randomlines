package eu.pintergabor.randomlines.ui;

import android.os.Bundle;

import androidx.preference.ListPreference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SeekBarPreference;
import androidx.preference.SwitchPreference;

import eu.pintergabor.randomlines.R;
import eu.pintergabor.randomlines.ui.helper.PrefsChangeListener;

/**
 * The core of settings.
 */
public class PrefsFragment extends PreferenceFragmentCompat {

    /**
     * Find a {@link ListPreference}.
     *
     * @param key Key.
     * @return The corresponding {@link ListPreference}.
     */
    @SuppressWarnings("SameParameterValue")
    public ListPreference findListPreference(String key){
        return (ListPreference)findPreference(key);
    }

    /**
     * Find a {@link SeekBarPreference}.
     *
     * @param key Key.
     * @return The corresponding {@link SeekBarPreference}.
     */
    @SuppressWarnings("SameParameterValue")
    public SeekBarPreference findSeekBarPreference(String key){
        return (SeekBarPreference)findPreference(key);
    }

    /**
     * Find a {@link SwitchPreference}.
     *
     * @param key Key.
     * @return The corresponding {@link SwitchPreference}.
     */
    @SuppressWarnings("SameParameterValue")
    public SwitchPreference findSwitchPreference(String key){
        return (SwitchPreference)findPreference(key);
    }

    /**
     * Inflate an initialize.
     */
    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        // Helper
        PrefsChangeListener helper = new PrefsChangeListener(this);
        // Inflate preferences
        setPreferencesFromResource(R.xml.preferences, s);
        // Get references
        final ListPreference drawingType = findListPreference("drawing_type");
        final SeekBarPreference lineWidth = findSeekBarPreference("line_width");
        final SeekBarPreference size = findSeekBarPreference("size");
        final SeekBarPreference touchStrength = findSeekBarPreference("touch_strength");
        final SeekBarPreference touchRandom = findSeekBarPreference("touch_random");
        final SwitchPreference freeRunning = findSwitchPreference("free_running");
        // Set DPAD key increments
        lineWidth.setSeekBarIncrement(1);
        size.setSeekBarIncrement(10);
        touchStrength.setSeekBarIncrement(10);
        touchRandom.setSeekBarIncrement(10);
        // Connect preferences to actions
        helper.tieChange(drawingType);
        helper.tieChange(lineWidth);
        helper.tieChange(size);
        helper.tieChange(touchStrength);
        helper.tieChange(touchRandom);
        helper.tieChange(freeRunning);
    }

}
