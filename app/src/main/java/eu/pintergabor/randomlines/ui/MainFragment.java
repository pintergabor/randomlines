package eu.pintergabor.randomlines.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.*;
import androidx.fragment.app.Fragment;

import eu.pintergabor.randomlines.R;

/**
 * Main view containing the image.
 */
public class MainFragment extends Fragment {

    @Override
    public View onCreateView(
        @NonNull LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        // Return
        return v;
    }

}
