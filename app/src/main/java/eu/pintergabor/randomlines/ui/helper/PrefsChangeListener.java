package eu.pintergabor.randomlines.ui.helper;

import android.content.SharedPreferences;

import androidx.annotation.*;
import androidx.preference.Preference;
import androidx.preference.PreferenceManager;

import eu.pintergabor.randomlines.com.MainHub;
import eu.pintergabor.randomlines.ui.MainActivity;
import eu.pintergabor.util.basic.Cast;
import eu.pintergabor.util.preferences.ValueToSummary;
import eu.pintergabor.randomlines.ui.PrefsFragment;

/**
 * Help {@link PrefsFragment}.
 */
public class PrefsChangeListener implements Preference.OnPreferenceChangeListener {

    /**
     * The  helped {@link PrefsFragment}.
     */
    private final PrefsFragment helped;

    /**
     * Help {@link PrefsFragment}.
     *
     * @param fragment The  helped {@link PrefsFragment}.
     */
    public PrefsChangeListener(@NonNull PrefsFragment fragment) {
        helped = fragment;
    }

    /**
     * Get {@link MainHub}.
     *
     * @return The only {@link MainHub} instance from {@link MainActivity}.
     */
    @NonNull
    private MainHub getMainHub() {
        return ((MainActivity) helped.requireActivity()).getMainHub();
    }

    /**
     * Called when a preference changes that might have effects on the main drawing.
     * <p/>
     * It should be private, because it is used only in {@link #tieChange(Preference)}.
     *
     * @return Always true = Always allow updating the preference to the new value.
     */
    @Override
    public boolean onPreferenceChange(Preference preference, Object value) {
        final MainHub mainHub = getMainHub();
        final String key = preference.getKey();
        switch (key) {
            case "drawing_type":
                // Update summary
                ValueToSummary.updateSummary(preference, value);
                // Change drawings
                mainHub.drawingType = Cast.ObjectToString(value);
                break;
            case "line_width":
                mainHub.lineWidth = Cast.ObjectToFloat(value);
                break;
            case "size":
                mainHub.size = Cast.ObjectToFloat(value) / 100f;
                break;
            case "touch_strength":
                mainHub.touchStrength = Cast.ObjectToFloat(value) / 100f;
                break;
            case "touch_random":
                mainHub.touchRandom = Cast.ObjectToFloat(value) / 100f;
                break;
            case "free_running":
                mainHub.freeRunning = Cast.ObjectToBoolean(value);
                break;
        }
        return true;
    }

    /**
     * Tie preference changes to actions.
     *
     * @param preference Some settings that might have an effect on the main drawing.
     */
    public void tieChange(@NonNull Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(this);
        // Trigger the listener immediately with the current value of the preference.
        SharedPreferences sp = PreferenceManager.
            getDefaultSharedPreferences(preference.getContext());
        Object value = sp.getAll().get(preference.getKey());
        onPreferenceChange(preference, value);
    }

}
