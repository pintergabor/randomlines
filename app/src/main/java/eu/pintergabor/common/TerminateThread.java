package eu.pintergabor.common;

/**
 * A {@link Thread} that is easy to terminate.
 */
@SuppressWarnings("unused")
public class TerminateThread extends Thread {

    /**
     * True if the thread has been started and not yet terminated.
     *
     * {@link #run()} should terminate when it is false.
     */
    private volatile boolean running;

    /**
     * Descendant shall terminate their {@link #run()}, when this returns false.
     * <p>
     * It is made public for the curious.
     *
     * @return True if the thread has been started and not yet terminated.
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * The standard way of terminating the thread.
     */
    public void terminateRun() {
        // Request termination.
        running = false;
        // Speed up termination.
        interrupt();
        // Wait for termination.
        try {
            join();
        } catch (InterruptedException e) {
            // Impossible
        }
    }

    /**
     * Start.
     */
    @Override
    public void start() {
        running = true;
        super.start();
    }

}
