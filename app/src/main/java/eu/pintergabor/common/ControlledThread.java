package eu.pintergabor.common;

/**
 * A {@link Thread} that is easy to terminate and pause.
 */
@SuppressWarnings("unused")
public class ControlledThread extends TerminateThread {

    /**
     * True if the thread is paused.
     * <p>
     * {@link #run()} should terminate when it is false.
     */
    private volatile boolean pausing;

    /**
     * Lock used to wait until {@link #pausing} gets false again.
     */
    private final Object pauseLock = new Object();

    /**
     * The thread is, or will be soon, paused when this returns true.
     * <p>
     * It is made public for the curious.
     *
     * @return True if the thread has been asked to pause.
     */
    public boolean isPausing() {
        synchronized (pauseLock) {
            return pausing;
        }
    }

    /**
     * Ask the thread to pause.
     */
    @SuppressWarnings("unused")
    public void pauseRun() {
        synchronized (pauseLock) {
            pausing = true;
        }
    }

    /**
     * Ask the thread to continue running.
     */
    @SuppressWarnings("unused")
    public void resumeRun() {
        synchronized (pauseLock) {
            pausing = false;
            pauseLock.notifyAll();
        }
    }

    /**
     * Descendant shall call this regularly from their {@link #run()},
     * when it is convenient to pause the thread.
     */
    protected void mayPause() {
        while (isPausing() && isRunning()) {
            try {
                synchronized (pauseLock) {
                    pauseLock.wait();
                }
            } catch (InterruptedException e) {
                // It is OK.
            }
        }
    }

}
