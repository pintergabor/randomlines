package eu.pintergabor.util.ui;

import androidx.annotation.*;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

/**
 * Encapsulate one {@link MutableLiveData}<{@link Integer}> value for easy access.
 */
@SuppressWarnings("unused")
public class LiveInt {

    /**
     * Storage for the number.
     */
    @NonNull
    protected final MutableLiveData<Integer> liveData = new MutableLiveData<>();

    /**
     * Called when the stored number changes.
     *
     * @param value New value.
     */
    @SuppressWarnings("unused")
    public synchronized void setValue(int value) {
        liveData.setValue(value);
    }

    /**
     * Get current value.
     *
     * @return the current value
     */
    @SuppressWarnings({"WeakerAccess", "unused", "ConstantConditions"})
    public synchronized int getValue() {
        return liveData.getValue();
    }

    /**
     * Used when something has to be done when the stored number changes.
     *
     * @return The storage where the changes can be observed.
     */
    @SuppressWarnings("unused")
    public LiveData<Integer> getObserver() {
        return liveData;
    }

}
