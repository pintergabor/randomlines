package eu.pintergabor.util.ui;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

/**
 * Encapsulate one {@link MutableLiveData} value for easy access.
 */
@SuppressWarnings("unused")
public class LiveObject<T> {

    /**
     * Storage for the number.
     */
    @NonNull
    protected final MutableLiveData<T> liveData = new MutableLiveData<>();

    /**
     * Called when the stored value changes.
     *
     * @param value New value.
     */
    @SuppressWarnings("unused")
    public synchronized void setValue(T value) {
        liveData.setValue(value);
    }

    /**
     * Get current value.
     *
     * @return the current value
     */
    @SuppressWarnings({"unused", "ConstantConditions"})
    public synchronized T getValue() {
        return liveData.getValue();
    }

    /**
     * Used when something has to be done when the stored value changes.
     *
     * @return The storage where the changes can be observed.
     */
    @SuppressWarnings("unused")
    public LiveData<T> getObserver() {
        return liveData;
    }

}
