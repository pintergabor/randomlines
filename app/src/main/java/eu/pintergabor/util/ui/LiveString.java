package eu.pintergabor.util.ui;

import androidx.lifecycle.MutableLiveData;

/**
 * Encapsulate one {@link MutableLiveData}<{@link String}> value for easy access.
 */
@SuppressWarnings("unused")
public class LiveString extends LiveObject<String> {
    // Nothing.
}
