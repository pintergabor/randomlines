package eu.pintergabor.util.view;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.view.View;

import org.jetbrains.annotations.Contract;

/**
 * Utilities primarily usable in {@link View}s.
 */
@SuppressWarnings("unused")
public final class AdditionalViewUtils {

    /**
     * It is a static class.
     */
    private AdditionalViewUtils() {
        // Nothing.
    }

    /**
     * Get hosting {@link Activity} from a {@link Context}.
     *
     * @param context Context.
     * @return the hosting activity, or null, if it cannot be determined.
     */
    @Contract("null -> null")
    @SuppressWarnings("unused")
    public static Activity getActivity(Context context) {
        if (context != null) {
            while (context instanceof ContextWrapper) {
                if (context instanceof Activity) {
                    return (Activity) context;
                }
                context = ((ContextWrapper) context).getBaseContext();
            }
        }
        return null;
    }

    /**
     * Get hosting {@link Activity} from a {@link View}.
     *
     * @param view View.
     * @return the hosting activity, or null, if it cannot be determined.
     */
    @Contract("null -> null")
    @SuppressWarnings("unused")
    public static Activity getActivity(View view) {
        if (view != null) {
            return getActivity(view.getContext());
        }
        return null;
    }

}
